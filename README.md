# Date Content Augmenter

This module leverages the Date Augmenter API to allow content to be associated
with Individual values in a multivalued or recurring date field. Although
designed to work with Smart Date, this functionality can be used with any field
supported by a compatible date formatter, which includes core date fields.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/date_content).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/date_content).


## Table of contents

- Requirements
- Installation
- Configuration
- Features
- Maintainers


## Requirements

This module requires the following modules:

- [Date Augmenter API](https://www.drupal.org/project/date_augmenter)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- The module creates an initial Date Content bundle called Session, to allow
  it to be used immediately after being installed. You can edit the fields at
  `/admin/structure/date_content_types/session/edit/fields`.
- If you prefer, you can delete the Session bundle and create a new Date
  Content type at `/admin/structure/date_content_types/add` and add whatever
  fields you need.
- In your date formatter, enable the Content augmenter.
- Configure how you want the forms for adding, editing, or deleting content to
  be linked: on a separate page (default), in the settings tray, or in a modal.
- If using the settings tray or a modal, you can also configure the width.


## Maintainers

- Martin Anderson-Clutz - [@mandclu](https://www.drupal.org/u/mandclu)
