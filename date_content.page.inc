<?php

/**
 * @file
 * Contains date_content.page.inc.
 *
 * Page callback for date_content entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Storage templates.
 *
 * Default template: storage.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_date_content(array &$variables) {
  // Fetch Date Content entity.
  $entity = $variables['elements']['#date_content'];
  $variables['date_content'] = $entity;

  // Helpful $content variable for templates.
  if (!empty($variables['elements'])) {
    foreach (Element::children($variables['elements']) as $key) {
      $variables['content'][$key] = $variables['elements'][$key];
    }
  }
}
