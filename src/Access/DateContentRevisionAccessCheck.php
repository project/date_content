<?php

namespace Drupal\date_content\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\date_content\Entity\DateContentInterface;
use Symfony\Component\Routing\Route;

/**
 * Provides an access checker for date_content revisions.
 *
 * @ingroup date_content_access
 */
class DateContentRevisionAccessCheck implements AccessInterface {

  /**
   * The date_content date_content.
   *
   * @var \Drupal\date_content\DateContentStorageInterface
   */
  protected $dateContentStorage;

  /**
   * The date_content access control handler.
   *
   * @var \Drupal\Core\Entity\EntityAccessControlHandlerInterface
   */
  protected $dateContentAccess;

  /**
   * A static cache of access checks.
   *
   * @var array
   */
  protected $access = [];

  /**
   * Constructs a new DateContentRevisionAccessCheck.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->dateContentStorage = $entity_type_manager->getStorage('date_content');
    $this->dateContentAccess = $entity_type_manager->getAccessControlHandler('date_content');
  }

  /**
   * Checks routing access for the date_content revision.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   * @param int $date_content_revision
   *   (optional) The date_content revision ID. If not specified, but $date_content is,
   *   access is checked for that object's revision.
   * @param \Drupal\date_content\DateContentInterface $date_content
   *   (optional) A date_content object. Used for checking access to a date_content's
   *   default revision when $date_content_revision is unspecified. Ignored when
   *   $date_content_revision is specified. If neither $date_content_revision nor
   *   $date_content are specified, then access is denied.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, AccountInterface $account, $date_content_revision = NULL, DateContentInterface $date_content = NULL) {
    if ($date_content_revision) {
      $date_content = $this->dateContentStorage->loadRevision($date_content_revision);
    }
    $operation = $route->getRequirement('_access_date_content_revision');
    return AccessResult::allowedIf($date_content && $this->checkAccess($date_content, $account, $operation))->cachePerPermissions()->addCacheableDependency($date_content);
  }

  /**
   * Checks date_content revision access.
   *
   * @param \Drupal\date_content\DateContentInterface $date_content
   *   The date_content to check.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   A user object representing the user for whom the operation is to be
   *   performed.
   * @param string $op
   *   (optional) The specific operation being checked. Defaults to 'view.'
   *
   * @return bool
   *   TRUE if the operation may be performed, FALSE otherwise.
   */
  public function checkAccess(DateContentInterface $date_content, AccountInterface $account, $op = 'view') {
    $map = [
      'view' => 'view all revisions',
      'update' => 'revert all revisions',
      'delete' => 'delete all revisions',
    ];
    $bundle = $date_content->bundle();
    $type_map = [
      'view' => "view $bundle revisions",
      'update' => "revert $bundle revisions",
      'delete' => "delete $bundle revisions",
    ];

    if (!$date_content || !isset($map[$op]) || !isset($type_map[$op])) {
      // If there was no date_content to check against, or the $op was not one of the
      // supported ones, we return access denied.
      return FALSE;
    }

    // Statically cache access by revision ID, language code, user account ID,
    // and operation.
    $langcode = $date_content->language()->getId();
    $cid = $date_content->getRevisionId() . ':' . $langcode . ':' . $account->id() . ':' . $op;

    if (!isset($this->access[$cid])) {
      // Perform basic permission checks first.
      if (!$account->hasPermission($map[$op]) && !$account->hasPermission($type_map[$op]) && !$account->hasPermission('administer date_contents')) {
        $this->access[$cid] = FALSE;
        return FALSE;
      }
      // If the revisions checkbox is selected for the date_content type, display the
      // revisions tab.
      $bundle_entity_type = $date_content->getEntityType()->getBundleEntityType();
      $bundle_entity = \Drupal::entityTypeManager()->getStorage($bundle_entity_type)->load($bundle);
      if ($bundle_entity->shouldCreateNewRevision() && $op === 'view') {
        $this->access[$cid] = TRUE;
      } else {
        // There should be at least two revisions. If the vid of the given date_content
        // and the vid of the default revision differ, then we already have two
        // different revisions so there is no need for a separate database
        // check. Also, if you try to revert to or delete the default revision,
        // that's not good.
        if ($date_content->isDefaultRevision() && ($this->dateContentStorage->countDefaultLanguageRevisions($date_content) == 1 || $op === 'update' || $op === 'delete')) {
          $this->access[$cid] = FALSE;
        } elseif ($account->hasPermission('administer date_contents')) {
          $this->access[$cid] = TRUE;
        } else {
          // First check the access to the default revision and finally, if the
          // date_content passed in is not the default revision then check access to
          // that, too.
          $this->access[$cid] = $this->dateContentAccess->access($this->dateContentStorage->load($date_content->id()), $op, $account) && ($date_content->isDefaultRevision() || $this->dateContentAccess->access($date_content, $op, $account));
        }
      }
    }

    return $this->access[$cid];
  }
}
