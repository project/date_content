<?php

namespace Drupal\date_content\Plugin\DateAugmenter;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Routing\RedirectDestinationTrait;
use Drupal\Core\Url;
use Drupal\date_augmenter\DateAugmenter\DateAugmenterPluginBase;
use Drupal\date_augmenter\Plugin\PluginFormTrait;

/**
 * Date Augmenter plugin to inject Registration links.
 *
 * @DateAugmenter(
 *   id = "content",
 *   label = @Translation("Content"),
 *   description = @Translation("Adds content to an event, for example to add a meeting presenter and topic."),
 *   weight = 0
 * )
 */
class Content extends DateAugmenterPluginBase implements PluginFormInterface {

  use PluginFormTrait;
  use RedirectDestinationTrait;

  protected $processService;
  protected $config;
  protected $output;

  /**
   * Builds and returns a render array for the task.
   *
   * @param array $output
   *   The existing render array, to be augmented, passed by reference.
   * @param Drupal\Core\Datetime\DrupalDateTime $start
   *   The object which contains the start time.
   * @param Drupal\Core\Datetime\DrupalDateTime $end
   *   The optionalobject which contains the end time.
   * @param array $options
   *   An array of options to further guide output.
   */
  public function augmentOutput(array &$output, DrupalDateTime $start, DrupalDateTime $end = NULL, array $options = []) {
    // Ensure we have required data.
    if (!isset($options['delta']) || !isset($options['entity'])) {
      return;
    }
    $config = $options['settings'] ?? $this->getConfiguration();
    $end_fallback = $end ?? $start;
    $now = new DrupalDateTIme();
    if ($end_fallback < $now && !$config['past_events']) {
      return;
    }
    $bundles = $config['bundles'];
    foreach ($bundles as $key => $value) {
      if (!$value) {
        unset($bundles[$key]);
      }
    }
    $all_bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo('date_content');
    if (empty($bundles)) {
      $bundles = array_keys($all_bundles);
    }
    // If no Date Content bundles exist, nothing to do.
    if (!$bundles) {
      return;
    }
    $user_can_admin = \Drupal::currentUser()->hasPermission('administer date content entities');
    $user_can_add = FALSE;
    $user_can_edit = FALSE;
    $new_output = [];
    $entity = $options['entity'];
    $field_name = $options['field_name'];
    // @todo Test if can use $this->entityTypeManager instead.
    $view_builder = \Drupal::entityTypeManager()->getViewBuilder('date_content');
    $entity_storage = \Drupal::entityTypeManager()->getStorage('date_content');
    foreach ($bundles as $bundle) {
      $query = \Drupal::entityQuery('date_content')
        ->condition('type', $bundle)
        ->condition('parent_id', $entity->id())
        ->condition('parent_type', $entity->getEntityTypeId())
        ->condition('field_name', $field_name)
        ->condition('field_delta', $options['delta'])
        ->accessCheck(FALSE);
      $existing = $query->execute();
      if ($existing) {
        $existing = array_shift($existing);
        $content = $entity_storage->load($existing);
        // Show the existing content and add link to edit if permited.
        $new_output[$bundle]['display'] = $view_builder->view($content, 'full');

        // Check for permission to edit.
        if (!$user_can_admin) {
          $user_can_edit = \Drupal::currentUser()->hasPermission('edit date content entities');
        }
        if ($user_can_admin || $user_can_edit) {
          $edit_url = Url::fromRoute('date_content.revise', ['date_content' => $content->id()]);
          $title = $this->t('Edit');
          $new_output[$bundle]['links']['edit'] = $this->structureLink($edit_url, $title, $config);
        }

        // Check for permission to delete.
        if (!$user_can_admin) {
          $user_can_delete = \Drupal::currentUser()->hasPermission('delete date content entities');
        }
        if ($user_can_admin || $user_can_delete) {
          $delete_url = Url::fromRoute('entity.date_content.delete_form', ['date_content' => $content->id()]);
          $title = $this->t('Remove');
          $new_output[$bundle]['links']['delete'] = $this->structureLink($delete_url, $title, $config);
        }
      }
      else {
        // Show link to add, if permitted.
        if (!$user_can_admin) {
          $user_can_add = \Drupal::currentUser()->hasPermission('add date content entities');
        }
        if ($user_can_admin || $user_can_add) {
          $add_url = Url::fromRoute(
            'date_content.add_form_param',
            [
              'date_content_type' => $bundle,
              'parent_type' => $entity->getEntityTypeId(),
              'parent_id' => $entity->id(),
              'field_name' => $field_name,
              'field_delta' => $options['delta'],
            ],
            [],
          );
          $bundle_label = $all_bundles[$bundle]['label'];
          // @todo use twig template or some other way to allow custom verbiage.
          $title = $this->t('Add @bundle content', ['@bundle' => $bundle_label]);
          $new_output[$bundle]['links']['add'] = $this->structureLink($add_url, $title, $config);
        }
      }
    }
    if ($new_output) {
      // @todo format using a twig template?
      $output['date_content'] = $new_output;
    }
  }

  /**
   * Construct a link with the configured attributes.
   *
   * @param Drupal\Core\Url $url
   *   Where the link should point.
   * @param string $title
   *   The text of the link.
   * @param array $config
   *   The configuration for the augmenter.
   *
   * @return array
   *   The render array for the assembled link.
   */
  protected function structureLink(Url $url, $title, array $config) {
    // Set a return path.
    $url->setOption('query', $this->getDestinationArray());
    // Add link to add content.
    $link = [
      '#title' => $title,
      '#type' => 'link',
      '#url' => $url,
      '#prefix' => ' ',
      '#suffix' => ' ',
    ];
    if (!empty($config['target'])) {
      $link['#options']['attributes']['class'][] = 'use-ajax';
      $width = $config['width'] ?: 600;
      $link['#options']['attributes']['data-dialog-options'] = Json::encode(['width' => $width]);
      switch ($config['target']) {
        case 'tray':
          $link['#options']['attributes']['data-dialog-renderer'] = 'off_canvas';
          $link['#options']['attributes']['data-dialog-type'] = 'dialog';
          break;

        case 'modal':
          $link['#options']['attributes']['data-dialog-type'] = 'modal';
          break;
      }
    }
    return $link;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'bundles' => [],
      'past_events' => TRUE,
      'target' => '',
      'width' => '600',
      'form_mode' => NULL,
    ];
  }

  /**
   * Create configuration fields for the plugin form, or injected directly.
   *
   * @param array $form
   *   The form array.
   * @param array $settings
   *   The setting to use as defaults.
   * @param mixed $field_definition
   *   A parameter to define the field being modified. Likely FieldConfig.
   *
   * @return array
   *   The updated form array.
   */
  public function configurationFields(array $form, ?array $settings, $field_definition) {
    if (empty($settings)) {
      $settings = $this->defaultConfiguration();
    }
    $all_bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo('date_content');
    $bundles = [];
    foreach ($all_bundles as $bundle_name => $bundle_info) {
      $bundles[$bundle_name] = $bundle_info['label'];
    }
    $form['bundles'] = [
      '#title' => $this->t('Date Content Bundles'),
      '#type' => 'checkboxes',
      '#default_value' => $settings['bundles'],
      '#options' => $bundles,
      '#description' => $this->t('The kind(s) of date content available to add. If none are selected, all available will be used.'),
    ];

    $form['past_events'] = [
      '#title' => $this->t('Show content and links for past events?'),
      '#type' => 'checkbox',
      '#default_value' => $settings['past_events'],
    ];

    $form['target'] = [
      '#title' => $this->t('Target'),
      '#description' => $this->t('Optionally have the form open on-page in a modal or off-canvas dialog.'),
      '#type' => 'select',
      '#default_value' => $settings['target'],
      '#options' => [
        '' => $this->t('Default'),
        'tray' => $this->t('Off-Screen Tray'),
        'modal' => $this->t('Modal Dialog'),
      ],
    ];

    $form['width'] = [
      '#title' => $this->t('Dialog Width'),
      '#description' => $this->t('How wide the dialog should appear.'),
      '#type' => 'number',
      '#min' => '100',
      '#default_value' => $settings['width'],
      '#states' => [
        // Show this number field only if a dialog is chosen above.
        'invisible' => [
          ':input[name="options[target]"]' => ['value' => ''],
        ],
      ],
    ];

    // If the Form Mode Control module is installed, expose an option to use it.
    if (\Drupal::service('module_handler')->moduleExists('form_mode_control')) {
      $form_modes = \Drupal::service('entity_display.repository')->getFormModeOptions('date_content');
      // Only expose the form element if our entity type has more than one
      // form mode.
      if ($form_modes && is_array($form_modes) && count($form_modes) > 1) {
        $form['form_mode'] = [
          '#title' => $this->t('Form mode'),
          '#description' => $this->t('The form mode to use for adding an entity.'),
          '#type' => 'select',
          '#options' => $form_modes,
          '#default_value' => (!empty($settings['form_mode'])) ? $settings['form_mode'] : '',
        ];
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $this->configurationFields($form, $this->configuration);

    return $form;
  }

}
