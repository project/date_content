<?php

namespace Drupal\date_content\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Date Content entities.
 *
 * @ingroup date_content
 */
class DateContentDeleteForm extends ContentEntityDeleteForm {


}
