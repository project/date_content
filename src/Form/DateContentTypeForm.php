<?php

namespace Drupal\date_content\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DateContentTypeForm.
 */
class DateContentTypeForm extends BundleEntityFormBase {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs the NodeTypeForm object.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager) {
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $date_content_type = $this->entity;
    if ($this->operation == 'add') {
      $form['#title'] = $this->t('Add date content type');
      $fields = $this->entityFieldManager->getBaseFieldDefinitions('date_content');
    } else {
      $form['#title'] = $this->t('Edit %label date content type', ['%label' => $date_content_type->label()]);
      $fields = $this->entityFieldManager->getFieldDefinitions('date_content', $date_content_type->id());
    }

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#maxlength' => 255,
      '#default_value' => $date_content_type->label(),
      '#description' => $this->t('The human-readable name of this date content type. This text will be displayed as part of the list on the <em>date_content date</em> page. This name must be unique.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $date_content_type->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#disabled' => !$date_content_type->isNew(),
      '#machine_name' => [
        'exists' => ['\Drupal\date_content\Entity\DateContentType', 'load'],
        'source' => ['label'],
      ],
      '#description' => $this->t('A unique machine-readable name for this date content type. It must only contain lowercase letters, numbers, and underscores. This name will be used for constructing the URL of the %date_content-add page.', [
        '%date_content-add' => $this->t('Add date content data'),
      ]),
    ];

    $form['description'] = [
      '#title' => $this->t('Description'),
      '#type' => 'textarea',
      '#default_value' => $date_content_type->getDescription(),
      '#description' => $this->t('This text will be displayed on the <em>Add new date content</em> page.'),
    ];

    $form['new_revision'] = [
      '#type' => 'checkbox',
      '#title' => t('Create new revision'),
      '#default_value' => $date_content_type->get('new_revision'),
      '#description' => t('Create a new revision by default for this date_content type.'),
    ];

    $form['revision_expose'] = [
      '#type' => 'checkbox',
      '#title' => t('Expose revision checkbox'),
      '#default_value' => $date_content_type->get('revision_expose'),
      '#description' => t('Whether or not a checkbox will be visible in the form. If not exposed, the default value above will always be used.'),
    ];

    $form['revision_log'] = [
      '#type' => 'checkbox',
      '#title' => t('Expose revision log'),
      '#default_value' => $date_content_type->get('revision_log'),
      '#description' => t('Whether or not the editor can write a revision log message.'),
      '#states' => [
        // Show this textarea only if the 'repeat' select has a value.
        'visible' => [
          'input[name="revision_expose"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $date_content_type = $this->entity;
    $status = $date_content_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Date Content type.', [
          '%label' => $date_content_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Date Content type.', [
          '%label' => $date_content_type->label(),
        ]));
    }

    $fields = $this->entityFieldManager->getFieldDefinitions('date_content', $date_content_type->id());
    // Update title field definition.
    $title_field = $fields['label'] ?? $fields['name'] ?? '';
    $title_label = $form_state->getValue('title_label');
    if ($title_field && $title_field->getLabel() != $title_label) {
      $title_field->getConfig($date_content_type->id())->setLabel($title_label)->save();
    }
    $form_state->setRedirectUrl($date_content_type->toUrl('collection'));
  }

}
