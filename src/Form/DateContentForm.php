<?php

namespace Drupal\date_content\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\date_content\Entity\DateContentType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Date Content edit forms.
 *
 * @ingroup date_content
 */
class DateContentForm extends ContentEntityForm {

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->account = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var \Drupal\date_content\Entity\DateContent $entity */
    $form = parent::buildForm($form, $form_state);

    /** @var \Drupal\date_content\DateContentInterface $entity */
    $entity = $this->entity;

    if ($entity->get('parent_type')->count()) {
      $parent_type = $entity->get('parent_type')->first()->getString();
      $parent_id = (int) $entity->get('parent_id')->first()->getString();
      // @todo validate parent values.
      $parent = \Drupal::entityTypeManager()->getStorage($parent_type)->load($parent_id);
      if ($parent) {
        $field_name = $entity->get('field_name')->getString();
        $field_delta = (int) $entity->field_delta->get(0)->getString();
        $parent_field_value = $parent->get($field_name)->get($field_delta)->view();
        if (!empty($parent_field_value['start'])) {
          $start = \Drupal::service('renderer')->render($parent_field_value);
        }
        else {
          // @todo Provide better fallback behaviour.
          $start = $field_delta;
        }

        if (!$entity->isNew()) {
          $form['#title'] = $this->t('<em>Edit @type</em> @title at @time', [
            '@type' => $entity->bundle(),
            '@title' => $parent->label(),
            '@time' => $start,
          ]);
        }
        else {
          $form['#title'] = $this->t('<em>New @type</em> for @title at @time', [
            '@type' => $entity->bundle(),
            '@title' => $parent->label(),
            '@time' => $start,
          ]);
        }
      }
    }
    $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions('date_content', $entity->bundle());

    // Load the bundle.
    $bundle = DateContentType::load($entity->bundle());

    $revision_default = $bundle->get('new_revision');

    // Only expose the log field if so configured.
    if (!$bundle->shouldShowRevisionLog()) {
      $form['revision_log']['#access'] = FALSE;
    }

    if ($bundle->shouldShowRevisionToggle()) {
      if (!$this->entity->isNew()) {
        $form['new_revision'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Create new revision'),
          '#default_value' => $revision_default,
          '#weight' => 10,
        ];
      }
    }
    else {
      $form['new_revision'] = [
        '#type' => 'value',
        '#value' => $revision_default,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('new_revision') && $form_state->getValue('new_revision') != FALSE) {
      $entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $entity->setRevisionCreationTime($this->time->getRequestTime());
      $entity->setRevisionUserId($this->account->id());
    }
    else {
      $entity->setNewRevision(FALSE);
    }

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Date Content.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Date Content.', [
          '%label' => $entity->label(),
        ]));
    }
  }

}
