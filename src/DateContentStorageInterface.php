<?php

namespace Drupal\date_content;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\date_content\Entity\DateContentInterface;

/**
 * Defines the date_content handler class for Date Content entities.
 *
 * This extends the base date_content class, adding required special handling
 * for Date Content entities.
 *
 * @ingroup date_content
 */
interface DateContentStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Date Content revision IDs for a specific Date Content.
   *
   * @param \Drupal\date_content\Entity\DateContentInterface $entity
   *   The Date Content entity.
   *
   * @return int[]
   *   Date Content revision IDs (in ascending order).
   */
  public function revisionIds(DateContentInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Date Content author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Date Content revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\date_content\Entity\DateContentInterface $entity
   *   The Date Content entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(DateContentInterface $entity);

  /**
   * Unsets the language for all Date Content with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
