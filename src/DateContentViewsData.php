<?php

namespace Drupal\date_content;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the date_content entity type.
 */
class DateContentViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['date_content_field_data']['table']['base']['weight'] = -10;
    $data['date_content_field_data']['table']['base']['access query tag'] = 'date_content_access';
    $data['date_content_field_data']['table']['wizard_id'] = 'date_content';

    $data['date_content_field_data']['id']['argument'] = [
      'id' => 'date_content_id',
      'name field' => 'title',
      'numeric' => TRUE,
      'validate type' => 'id',
    ];

    $data['date_content_field_data']['title']['field']['default_formatter_settings'] = ['link_to_entity' => TRUE];

    $data['date_content_field_data']['title']['field']['link_to_date_content default'] = TRUE;

    $data['date_content_field_data']['type']['argument']['id'] = 'date_content_type';

    $data['date_content_field_data']['langcode']['help'] = $this->t('The language of the data or translation.');

    $data['date_content']['date_content_bulk_form'] = [
      'title' => $this->t('Date Content operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple date content.'),
      'field' => [
        'id' => 'date_content_bulk_form',
      ],
    ];

    // Bogus fields for aliasing purposes.

    // @todo Add similar support to any date field
    // @see https://www.drupal.org/date_content/2337507
    $data['date_content_field_data']['created_fulldate'] = [
      'title' => $this->t('Created date'),
      'help' => $this->t('Date in the form of CCYYMMDD.'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_fulldate',
      ],
    ];

    $data['date_content_field_data']['created_year_month'] = [
      'title' => $this->t('Created year + month'),
      'help' => $this->t('Date in the form of YYYYMM.'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_year_month',
      ],
    ];

    $data['date_content_field_data']['created_year'] = [
      'title' => $this->t('Created year'),
      'help' => $this->t('Date in the form of YYYY.'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_year',
      ],
    ];

    $data['date_content_field_data']['created_month'] = [
      'title' => $this->t('Created month'),
      'help' => $this->t('Date in the form of MM (01 - 12).'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_month',
      ],
    ];

    $data['date_content_field_data']['created_day'] = [
      'title' => $this->t('Created day'),
      'help' => $this->t('Date in the form of DD (01 - 31).'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_day',
      ],
    ];

    $data['date_content_field_data']['created_week'] = [
      'title' => $this->t('Created week'),
      'help' => $this->t('Date in the form of WW (01 - 53).'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_week',
      ],
    ];

    $data['date_content_field_data']['changed_fulldate'] = [
      'title' => $this->t('Updated date'),
      'help' => $this->t('Date in the form of CCYYMMDD.'),
      'argument' => [
        'field' => 'changed',
        'id' => 'date_fulldate',
      ],
    ];

    $data['date_content_field_data']['changed_year_month'] = [
      'title' => $this->t('Updated year + month'),
      'help' => $this->t('Date in the form of YYYYMM.'),
      'argument' => [
        'field' => 'changed',
        'id' => 'date_year_month',
      ],
    ];

    $data['date_content_field_data']['changed_year'] = [
      'title' => $this->t('Updated year'),
      'help' => $this->t('Date in the form of YYYY.'),
      'argument' => [
        'field' => 'changed',
        'id' => 'date_year',
      ],
    ];

    $data['date_content_field_data']['changed_month'] = [
      'title' => $this->t('Updated month'),
      'help' => $this->t('Date in the form of MM (01 - 12).'),
      'argument' => [
        'field' => 'changed',
        'id' => 'date_month',
      ],
    ];

    $data['date_content_field_data']['changed_day'] = [
      'title' => $this->t('Updated day'),
      'help' => $this->t('Date in the form of DD (01 - 31).'),
      'argument' => [
        'field' => 'changed',
        'id' => 'date_day',
      ],
    ];

    $data['date_content_field_data']['changed_week'] = [
      'title' => $this->t('Updated week'),
      'help' => $this->t('Date in the form of WW (01 - 53).'),
      'argument' => [
        'field' => 'changed',
        'id' => 'date_week',
      ],
    ];

    $data['date_content_field_data']['uid']['help'] = $this->t('The user authoring the data. If you need more fields than the uid add the data: author relationship');
    $data['date_content_field_data']['uid']['filter']['id'] = 'user_name';
    $data['date_content_field_data']['uid']['relationship']['title'] = $this->t('Data author');
    $data['date_content_field_data']['uid']['relationship']['help'] = $this->t('Relate data to the user who created it.');
    $data['date_content_field_data']['uid']['relationship']['label'] = $this->t('author');

    $data['date_content']['date_content_listing_empty'] = [
      'title' => $this->t('Empty Date Content Frontpage behavior'),
      'help' => $this->t('Provides a link to the date_content add overview page.'),
      'area' => [
        'id' => 'date_content_listing_empty',
      ],
    ];

    $data['date_content_field_data']['uid_revision']['title'] = $this->t('User has a revision');
    $data['date_content_field_data']['uid_revision']['help'] = $this->t('All date_contents where a certain user has a revision');
    $data['date_content_field_data']['uid_revision']['real field'] = 'id';
    $data['date_content_field_data']['uid_revision']['filter']['id'] = 'date_content_uid_revision';
    $data['date_content_field_data']['uid_revision']['argument']['id'] = 'date_content_uid_revision';

    $data['date_content_field_revision']['table']['wizard_id'] = 'date_content_revision';

    // Advertise this table as a possible base table.
    $data['date_content_field_revision']['table']['base']['help'] = $this->t('Data revision is a history of changes to data.');
    $data['date_content_field_revision']['table']['base']['defaults']['title'] = 'title';

    $data['date_content_field_revision']['id']['argument'] = [
      'id' => 'date_content_id',
      'numeric' => TRUE,
    ];
    // @todo the NID field needs different behavior on revision/non-revision
    //   tables. It would be neat if this could be encoded in the base field
    //   definition.
    $data['date_content_field_revision']['id']['relationship']['id'] = 'standard';
    $data['date_content_field_revision']['id']['relationship']['base'] = 'date_content_field_data';
    $data['date_content_field_revision']['id']['relationship']['base field'] = 'id';
    $data['date_content_field_revision']['id']['relationship']['title'] = $this->t('Content');
    $data['date_content_field_revision']['id']['relationship']['label'] = $this->t('Get the actual data from a data revision.');
    $data['date_content_field_revision']['id']['relationship']['extra'][] = [
      'field' => 'langcode',
      'left_field' => 'langcode',
    ];

    $data['date_content_field_revision']['vid'] = [
      'argument' => [
        'id' => 'date_content_vid',
        'numeric' => TRUE,
      ],
      'relationship' => [
        'id' => 'standard',
        'base' => 'date_content_field_data',
        'base field' => 'vid',
        'title' => $this->t('Content'),
        'label' => $this->t('Get the actual data from a data revision.'),
        'extra' => [
          [
            'field' => 'langcode',
            'left_field' => 'langcode',
          ],
        ],
      ],
    ] + $data['date_content_field_revision']['vid'];

    $data['date_content_field_revision']['langcode']['help'] = $this->t('The language the original data is in.');

    $data['date_content_revision']['revision_uid']['help'] = $this->t('The user who created the revision.');
    $data['date_content_revision']['revision_uid']['relationship']['label'] = $this->t('revision user');
    $data['date_content_revision']['revision_uid']['filter']['id'] = 'user_name';

    $data['date_content_revision']['table']['join']['date_content_field_data']['left_field'] = 'vid';
    $data['date_content_revision']['table']['join']['date_content_field_data']['field'] = 'vid';

    $data['date_content_field_revision']['table']['wizard_id'] = 'date_content_field_revision';

    $data['date_content_field_revision']['langcode']['help'] = $this->t('The language of the data or translation.');

    $data['date_content_field_revision']['link_to_revision'] = [
      'field' => [
        'title' => $this->t('Link to revision'),
        'help' => $this->t('Provide a simple link to the revision.'),
        'id' => 'date_content_revision_link',
        'click sortable' => FALSE,
      ],
    ];

    $data['date_content_field_revision']['revert_revision'] = [
      'field' => [
        'title' => $this->t('Link to revert revision'),
        'help' => $this->t('Provide a simple link to revert to the revision.'),
        'id' => 'date_content_revision_link_revert',
        'click sortable' => FALSE,
      ],
    ];

    $data['date_content_field_revision']['delete_revision'] = [
      'field' => [
        'title' => $this->t('Link to delete revision'),
        'help' => $this->t('Provide a simple link to delete the data revision.'),
        'id' => 'date_content_revision_link_delete',
        'click sortable' => FALSE,
      ],
    ];

    // Define the base group of this table. Fields that don't have a group defined
    // will go into this field by default.
    $data['date_content_access']['table']['group'] = $this->t('Content access');

    // For other base tables, explain how we join.
    $data['date_content_access']['table']['join'] = [
      'date_content_field_data' => [
        'left_field' => 'id',
        'field' => 'id',
      ],
    ];
    $data['date_content_access']['id'] = [
      'title' => $this->t('Access'),
      'help' => $this->t('Filter by access.'),
      'filter' => [
        'id' => 'date_content_access',
        'help' => $this->t('Filter for data by view access. <strong>Not necessary if you are using date_content as your base table.</strong>'),
      ],
    ];

    // Add search table, fields, filters, etc., but only if a page using the
    // date_content_search plugin is enabled.
    if (\Drupal::moduleHandler()->moduleExists('search')) {
      $enabled = FALSE;
      $search_page_repository = \Drupal::service('search.search_page_repository');
      foreach ($search_page_repository->getActiveSearchpages() as $page) {
        if ($page->getPlugin()->getPluginId() == 'date_content_search') {
          $enabled = TRUE;
          break;
        }
      }

      if ($enabled) {
        $data['date_content_search_index']['table']['group'] = $this->t('Search');

        // Automatically join to the date_content table (or actually, date_content_field_data).
        // Use a Views table alias to allow other modules to use this table too,
        // if they use the search index.
        $data['date_content_search_index']['table']['join'] = [
          'date_content_field_data' => [
            'left_field' => 'id',
            'field' => 'sid',
            'table' => 'search_index',
            'extra' => "date_content_search_index.type = 'date_content_search' AND date_content_search_index.langcode = date_content_field_data.langcode",
          ],
        ];

        $data['date_content_search_total']['table']['join'] = [
          'date_content_search_index' => [
            'left_field' => 'word',
            'field' => 'word',
          ],
        ];

        $data['date_content_search_dataset']['table']['join'] = [
          'date_content_field_data' => [
            'left_field' => 'sid',
            'left_table' => 'date_content_search_index',
            'field' => 'sid',
            'table' => 'search_dataset',
            'extra' => 'date_content_search_index.type = date_content_search_dataset.type AND date_content_search_index.langcode = date_content_search_dataset.langcode',
            'type' => 'INNER',
          ],
        ];

        $data['date_content_search_index']['score'] = [
          'title' => $this->t('Score'),
          'help' => $this->t('The score of the search item. This will not be used if the search filter is not also present.'),
          'field' => [
            'id' => 'search_score',
            'float' => TRUE,
            'no group by' => TRUE,
          ],
          'sort' => [
            'id' => 'search_score',
            'no group by' => TRUE,
          ],
        ];

        $data['date_content_search_index']['keys'] = [
          'title' => $this->t('Search Keywords'),
          'help' => $this->t('The keywords to search for.'),
          'filter' => [
            'id' => 'search_keywords',
            'no group by' => TRUE,
            'search_type' => 'date_content_search',
          ],
          'argument' => [
            'id' => 'search',
            'no group by' => TRUE,
            'search_type' => 'date_content_search',
          ],
        ];

      }
    }

    return $data;
  }

}
