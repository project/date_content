<?php

namespace Drupal\date_content;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\date_content\Entity\DateContentInterface;

/**
 * Defines the date_content handler class for Date Content entities.
 *
 * This extends the base date_content class, adding required special handling
 * for Date Content entities.
 *
 * @ingroup date_content
 */
class DateContentStorage extends SqlContentEntityStorage implements DateContentStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(DateContentInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {date_content_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {date_content_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(DateContentInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {date_content_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('date_content_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
