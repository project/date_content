<?php

namespace Drupal\date_content;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for date_content.
 */
class DateContentTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
