<?php

namespace Drupal\date_content\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\date_content\Entity\DateContentInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DateContentController.
 *
 *  Returns responses for Date Content routes.
 */
class DateContentController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function addByParam($date_content_type, $parent_type, $parent_id, $field_name, $field_delta) {
    // Create a block entity with prepopulated values.
    $entity = \Drupal::entityTypeManager()->getStorage('date_content')->create([
      'parent_type' => $parent_type,
      'parent_id' => $parent_id,
      'field_name' => $field_name,
      'field_delta' => $field_delta,
      'type' => $date_content_type,
    ]);

    // @todo use a configured option for the form mode?
    $form = \Drupal::service('entity.form_builder')->getForm($entity, 'default');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function revise(DateContentInterface $date_content) {
    // @todo use a configured option for the form mode?
    $form = \Drupal::service('entity.form_builder')->getForm($date_content, 'default');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Date Content revision.
   *
   * @param int $date_content_revision
   *   The Date Content revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($date_content_revision) {
    /** @var \Drupal\Core\Entity\RevisionableStorageInterface $entity_type_manager */
    $entity_type_manager = $this->entityTypeManager();
    $date_content = $entity_type_manager->getStorage('date_content')
      ->loadRevision($date_content_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('date_content');

    return $view_builder->view($date_content);
  }

  /**
   * Page title callback for a Date Content revision.
   *
   * @param int $date_content_revision
   *   The Date Content revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($date_content_revision) {
    /** @var \Drupal\Core\Entity\RevisionableStorageInterface $entity_type_manager */
    $entity_type_manager = $this->entityTypeManager();
    $date_content = $entity_type_manager->getStorage('date_content')
      ->loadRevision($date_content_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $date_content->label(),
      '%date' => $this->dateFormatter->format($date_content->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Date Content entity.
   *
   * @param \Drupal\date_content\Entity\DateContentInterface $date_content
   *   A Date Content object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(DateContentInterface $date_content) {
    $account = $this->currentUser();
    /** @var \Drupal\Core\Entity\RevisionableStorageInterface $entity_type_manager */
    $entity_type_manager = $this->entityTypeManager();
    $date_content_date_content = $entity_type_manager->getStorage('date_content');

    $langcode = $date_content->language()->getId();
    $langname = $date_content->language()->getName();
    $languages = $date_content->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $date_content->label()]) : $this->t('Revisions for %title', ['%title' => $date_content->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all date_content revisions") || $account->hasPermission('administer date_content entities')));
    $delete_permission = (($account->hasPermission("delete all date_content revisions") || $account->hasPermission('administer date_content entities')));

    $rows = [];

    $vids = $date_content_date_content->revisionIds($date_content);

    $latest_revision = TRUE;
    $default_revision = $date_content->getRevisionId();
    $current_revision_displayed = FALSE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\date_content\DateContentInterface $revision */
      $revision = $date_content_date_content->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        $is_current_revision = $vid == $default_revision || (!$current_revision_displayed && $revision->wasDefaultRevision());
        if (!$is_current_revision) {
          $link = Link::fromTextAndUrl($date, new Url('entity.date_content.revision', ['date_content' => $date_content->id(), 'date_content_revision' => $vid]))->toString();
        } else {
          $link = $date_content->toLink($date)->toString();
          $current_revision_displayed = TRUE;
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.date_content.translation_revert', [
                'date_content' => $date_content->id(),
                'date_content_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.date_content.revision_revert', [
                'date_content' => $date_content->id(),
                'date_content_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.date_content.revision_delete', [
                'date_content' => $date_content->id(),
                'date_content_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['date_content_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
