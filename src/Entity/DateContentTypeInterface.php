<?php

namespace Drupal\date_content\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Date Content type entities.
 */
interface DateContentTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
