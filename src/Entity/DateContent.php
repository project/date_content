<?php

namespace Drupal\date_content\Entity;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\RevisionLogEntityTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\user\UserInterface;

/**
 * Defines the Date Content entity.
 *
 * @ingroup date_content
 *
 * @ContentEntityType(
 *   id = "date_content",
 *   label = @Translation("Date Content"),
 *   bundle_label = @Translation("Date Content type"),
 *   handlers = {
 *     "storage" = "Drupal\date_content\DateContentStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\date_content\DateContentListBuilder",
 *     "views_data" = "Drupal\date_content\Entity\DateContentViewsData",
 *     "translation" = "Drupal\date_content\DateContentTranslationHandler",
 *     "form" = {
 *       "default" = "Drupal\date_content\Form\DateContentForm",
 *       "add" = "Drupal\date_content\Form\DateContentForm",
 *       "edit" = "Drupal\date_content\Form\DateContentForm",
 *       "delete" = "Drupal\date_content\Form\DateContentDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\date_content\DateContentHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\date_content\DateContentAccessControlHandler",
 *   },
 *   base_table = "date_content",
 *   data_table = "date_content_field_data",
 *   revision_table = "date_content_revision",
 *   revision_data_table = "date_content_field_revision",
 *   translatable = TRUE,
 *   common_reference_target = TRUE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer date_content entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "parent_type",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "canonical" = "/date_content/{date_content}",
 *     "add-page" = "/date_content/add",
 *     "add-form" = "/date_content/add/{date_content_type}",
 *     "edit-form" = "/date_content/{date_content}/edit",
 *     "delete-form" = "/date_content/{date_content}/delete",
 *     "delete-multiple-form" = "/date_content/delete",
 *     "version-history" = "/date_content/{date_content}/revisions",
 *     "revision" = "/date_content/{date_content}/revisions/{date_content_revision}/view",
 *     "revision_revert" = "/date_content/{date_content}/revisions/{date_content_revision}/revert",
 *     "revision_delete" = "/date_content/{date_content}/revisions/{date_content_revision}/delete",
 *     "translation_revert" = "/date_content/{date_content}/revisions/{date_content_revision}/revert/{langcode}",
 *     "collection" = "/admin/content/date_content",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   bundle_entity_type = "date_content_type",
 *   field_ui_base_route = "entity.date_content_type.edit_form",
 *   fieldable = TRUE
 * )
 */
class DateContent extends ContentEntityBase implements DateContentInterface {

  use EntityChangedTrait;
  use RevisionLogEntityTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $date_content_controller, array &$values) {
    parent::preCreate($date_content_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $date_content) {
    parent::preSave($date_content);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the date_content owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    $this->invalidateTagsOnSave($update);
    // Make sure the parent entity is refreshed.
    $parent_type = $this->get('parent_type')->value;
    $parent_id = $this->get('parent_id')->target_id;
    Cache::invalidateTags([$parent_type . ':' . $parent_id]);
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    $parent_type = $this->get('parent_type')->value;
    if (!$parent_type) {
      return $this->bundle();
    }
    $label = $parent_type;
    $parent_id = $this->get('parent_id')->target_id;
    $parent = \Drupal::entityTypeManager()->getStorage($parent_type)->load($parent_id);
    if ($parent) {
      $field_name = $this->get('field_name')->getString();
      // $field_delta = $entity->get('field_delta')->get(0);
      $field_delta = (int) $this->field_delta->get(0)->getString();
      $parent_field_value = $parent->get($field_name)->get($field_delta)->view();
      if (!empty($parent_field_value['start'])) {
        $start = \Drupal::service('renderer')->render($parent_field_value);
      }
      else {
        // @todo Provide better fallback behaviour.
        $start = $field_delta;
      }

      $label = $this->t('@type for @title at @time', [
          '@type' => $this->bundle(),
          '@title' => $parent->label(),
          '@time' => $start,
        ]);
    }
    return $label;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    // Add the revision base fields.
    $fields += static::revisionLogBaseFieldDefinitions($entity_type);

    $fields['parent_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Parent entity type'))
      ->setRequired(TRUE)
      ->setDescription(t('The entity type to which this comment is attached.'))
      ->setSetting('is_ascii', TRUE)
      ->setSetting('max_length', EntityTypeInterface::ID_MAX_LENGTH);

    $fields['parent_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Parent entity ID'))
      ->setDescription(t('The ID of the entity whose date the content is being added to.'))
      ->setRequired(TRUE);

    $fields['field_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Reference field name'))
      ->setDescription(t('The field name whose value is being added to.'))
      ->setSetting('is_ascii', TRUE)
      ->setSetting('max_length', FieldStorageConfig::NAME_MAX_LENGTH);

    $fields['field_delta'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Smart Date field name'))
      ->setDescription(t('The field value whose value is being added to.'))
      ->setSetting('max_length', FieldStorageConfig::NAME_MAX_LENGTH);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Storage entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'region' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'region' => 'hidden',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Date Content entity.'))
      ->setReadOnly(TRUE);

    return $fields;
  }

}
