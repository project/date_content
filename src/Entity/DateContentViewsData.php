<?php

namespace Drupal\date_content\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Date Content entities.
 */
class DateContentViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['date_content_field_data']['table']['wizard_id'] = 'date_content';
    $data['date_content_field_revision']['table']['wizard_id'] = 'date_content_revision';

    $data['date_content_revision']['revision_user']['help'] = $this->t('The user who created the revision.');
    $data['date_content_revision']['revision_user']['relationship']['label'] = $this->t('revision user');
    $data['date_content_revision']['revision_user']['filter']['id'] = 'user_name';

    $data['date_content_revision']['table']['join']['date_content_field_data']['left_field'] = 'vid';
    $data['date_content_revision']['table']['join']['date_content_field_data']['field'] = 'vid';

    return $data;
  }

}
