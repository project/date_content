<?php

namespace Drupal\date_content\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the Date Content type entity.
 *
 * @ConfigEntityType(
 *   id = "date_content_type",
 *   label = @Translation("Date Content type"),
 *   label_collection = @Translation("Date Content types"),
 *   label_singular = @Translation("date_content type"),
 *   label_plural = @Translation("date_content types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count date_content type",
 *     plural = "@count date_content types",
 *   ),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\date_content\DateContentTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\date_content\Form\DateContentTypeForm",
 *       "edit" = "Drupal\date_content\Form\DateContentTypeForm",
 *       "delete" = "Drupal\date_content\Form\DateContentTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\date_content\DateContentTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "date_content_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "date_content",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/date_content_types/{date_content_type}",
 *     "add-form" = "/admin/structure/date_content_types/add",
 *     "edit-form" = "/admin/structure/date_content_types/{date_content_type}/edit",
 *     "delete-form" = "/admin/structure/date_content_types/{date_content_type}/delete",
 *     "collection" = "/admin/structure/date_content_types"
 *   },
 *   config_export = {
 *     "label",
 *     "id",
 *     "description",
 *     "help",
 *     "new_revision",
 *     "revision_expose",
 *     "revision_log",
 *   }
 * )
 */
class DateContentType extends ConfigEntityBundleBase implements DateContentTypeInterface {

  /**
   * The Date Content type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Date Content type label.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of this node type.
   *
   * @var string
   */
  protected $description;

  /**
   * Help information shown to the user when creating date_content data of this type.
   *
   * @var string
   */
  protected $help;

  /**
   * Default value of the 'Create new revision' checkbox of this date_content type.
   *
   * @var bool
   */
  protected $new_revision = TRUE;

  /**
   * Whether to show the 'Create new revision' checkbox of this date_content type.
   *
   * @var bool
   */
  protected $revision_expose = FALSE;

  /**
   * Whether to show the revision log for this date_content type.
   *
   * @var bool
   */
  protected $revision_log = FALSE;

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function isLocked() {
    $locked = \Drupal::state()->get('date_content.type.locked');
    return isset($locked[$this->id()]) ? $locked[$this->id()] : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function setNewRevision($new_revision) {
    $this->new_revision = $new_revision;
  }

  /**
   * {@inheritdoc}
   */
  public function getHelp() {
    return $this->help;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $date_content, $update = TRUE) {
    parent::postSave($date_content, $update);
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $date_content, array $entities) {
    parent::postDelete($date_content, $entities);

    // Clear the node type cache to reflect the removal.
    $date_content->resetCache(array_keys($entities));
  }

  /**
   * {@inheritdoc}
   */
  public function shouldCreateNewRevision() {
    return $this->new_revision;
  }

  /**
   * {@inheritdoc}
   */
  public function shouldShowRevisionToggle() {
    return $this->revision_expose;
  }

  /**
   * {@inheritdoc}
   */
  public function shouldShowRevisionLog() {
    return $this->revision_log;
  }
}
