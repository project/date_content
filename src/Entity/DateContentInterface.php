<?php

namespace Drupal\date_content\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface for defining Date Content entities.
 *
 * @ingroup date_content
 */
interface DateContentInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Date Content name.
   *
   * @return string
   *   Name of the Date Content.
   */
  // public function getName();

  /**
   * Sets the Date Content name.
   *
   * @param string $name
   *   The Date Content name.
   *
   * @return \Drupal\date_content\Entity\DateContentInterface
   *   The called Date Content entity.
   */
  // public function setName($name);

  /**
   * Gets the Date Content creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Date Content.
   */
  // public function getCreatedTime();

  /**
   * Sets the Date Content creation timestamp.
   *
   * @param int $timestamp
   *   The Date Content creation timestamp.
   *
   * @return \Drupal\date_content\Entity\DateContentInterface
   *   The called Date Content entity.
   */
  // public function setCreatedTime($timestamp);

  /**
   * Gets the Date Content revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  // public function getRevisionCreationTime();

  /**
   * Sets the Date Content revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\date_content\Entity\DateContentInterface
   *   The called Date Content entity.
   */
  // public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Date Content revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  // public function getRevisionUser();

  /**
   * Sets the Date Content revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\date_content\Entity\DateContentInterface
   *   The called Date Content entity.
   */
  // public function setRevisionUserId($uid);

}
